import { MobiquityPage } from './app.po';

describe('mobiquity App', () => {
  let page: MobiquityPage;

  beforeEach(() => {
    page = new MobiquityPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
