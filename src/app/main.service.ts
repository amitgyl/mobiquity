import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { IItem } from './models';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MainService {
  private apiURL = 'http://ergast.com/api/f1/driverStandings/1.json?limit=11&offset=55';
  private apiURL1 = 'http://ergast.com/api/f1';
  constructor(private http: Http) { }
  items$: Subject<IItem[]> = new Subject();

  // tslint:disable-next-line:max-line-length
  /* In the below funtion I am getting list of champions from year 2005 to 2015. For this I have modified the normal API URL. I conisdered getting the start and end yar as argument and then getting data for those specific years from the API but it seems that it is not provided. Another way to do this would have been that I called the endpoint for each from 2005 to 2015 using a for look here and then combined the responses into a single response using a fork join. But I decided to go with the former approach, i.e. using the modified URL. */

  public getChampions(): void {
    this.http.get(`${this.apiURL}`).subscribe(response => {
      const jsonRes = JSON.parse(response['_body']);
      console.log(jsonRes.MRData.StandingsTable.StandingsLists);
      const items: IItem[] = [];
      jsonRes.MRData.StandingsTable.StandingsLists.forEach((season, index) => {
        const item: IItem = {
          year: season.season,
          driverId : season.DriverStandings[0].Driver.driverId,
          driverName: season.DriverStandings[0].Driver.givenName + ' ' + season.DriverStandings[0].Driver.familyName,
          isSelected: (!index)
        };
        items.push(item);
      });
      console.log(items);
      this.items$.next(items);
    });
  }

  // tslint:disable-next-line:max-line-length
  /* In the above function when I got the champions I had created a model and I mapped the response into an array fo that model. That is because I knew that I was going to show just name and year. However in the below function while getting details I decided to return the observable responseas it is. I usually prefer not to use this approach (i.e. use 'any') but just wanted to show 2 different ways of doing it */
  public getYearData(year: string): Observable<Response> {
    const position = 1;
    return this.http.get(`${this.apiURL1}/${year}/results/${position}.json`);
  }
}
