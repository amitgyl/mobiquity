import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { ItemComponent } from './item/item.component';
import { MainService } from './main.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

describe('AppComponent', () => {

  const MockMainService  = {};
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ItemComponent
      ],
      providers: [
        { provide: MainService, useValue: MockMainService }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
