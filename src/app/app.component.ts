import { Component, OnInit } from '@angular/core';
import { MainService } from './main.service';
import { IItem } from './models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  items: IItem[];
  yearData: any[];
  private query_request: any;
  constructor(private _main: MainService) {}
  ngOnInit() {
    this._main.items$.subscribe(items => {
      this.items = items;
      this.itemSelected(this.items[0]);
    });
    this._main.getChampions();
  }

  itemSelected (e: IItem) {
    this.items.map(item => {
      item.isSelected = (item.year === e.year);
      return item;
    });
    console.log('item selected', e, this.items);
    if (this.query_request) {
      this.query_request.unsubscribe();
    }
    this.query_request = this._main.getYearData(e.year).subscribe(
    response => {
        const jsonRes = JSON.parse(response['_body']);
        console.log(jsonRes.MRData.RaceTable.Races);
        this.yearData = jsonRes.MRData.RaceTable.Races;
      });
  }

  isWinner(driver) {
    const driverDetails =  this.items.find(item =>
      (item.driverId === driver.Results[0].Driver.driverId)
      && item.year === driver.season );
    if (driverDetails) {
      return driverDetails.isSelected;
    } else {
      return false;
    }
  }
}
