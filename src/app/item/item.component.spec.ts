import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ItemComponent } from './item.component';
import { DebugElement } from '@angular/core';
import { IItem } from '../models';

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;
  let el: DebugElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.query(By.css('#child'));
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set the values', () => {
    component.item = {
      year: '2005',
      isSelected: false,
      driverId: 'alonso',
      driverName: 'Fernando Alonso'
    };
    fixture.detectChanges();
    expect(el.nativeElement.textContent).toContain('2005 : Fernando Alonso');
    expect(el.nativeElement.classList.contains('selected')).toBe(false);
  });

  it('should have the selected class if isSelected is true', () => {
    component.item = {
      year: '2005',
      isSelected: true,
      driverId: 'alonso',
      driverName: 'Fernando Alonso'
    };
    fixture.detectChanges();
    expect(el.nativeElement.classList.contains('selected')).toBe(true);
  });

  it('should not have the selected class if isSelected is false', () => {
    component.item = {
      year: '2005',
      isSelected: false,
      driverId: 'alonso',
      driverName: 'Fernando Alonso'
    };
    fixture.detectChanges();
    expect(el.nativeElement.classList.contains('selected')).toBe(false);
  });

  it('should emit an event with driver as the object', () => {
    component.item = {
      year: '2005',
      isSelected: false,
      driverId: 'alonso',
      driverName: 'Fernando Alonso'
    };
    // tslint:disable-next-line:prefer-const
    let driver: IItem;
    fixture.detectChanges();
    component.selectItem.subscribe((value) => driver = value);
    el.triggerEventHandler('click', null);
    expect(driver.driverId).toBe('alonso');
    expect(driver.driverName).toBe('Fernando Alonso');
  });
});
