import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IItem } from '../models';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent   {
  @Input() item: IItem;
  @Output() selectItem: EventEmitter<IItem> = new EventEmitter();

  onclick() {
    // console.log(this.item);
    this.selectItem.emit(this.item);
  }
}
