export interface IItem {
    year: string;
    driverName: string;
    driverId: string;
    isSelected?: boolean;
}
